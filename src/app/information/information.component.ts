import { Component } from '@angular/core';
import { environment } from '../../environments/environment';

declare const SwaggerUIBundle: any;

@Component({
  selector: 'ign-information',
  templateUrl: 'information.component.html',
  styleUrls: ['information.component.scss']
})

/**
 * Information Component contains documentation about the Ignition App.
 */
export class InformationComponent {
}
